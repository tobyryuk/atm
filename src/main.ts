import Bills from './bills'

export default class ATM {
  bills: Bills

  constructor(bills: Bills = {
    1000: 2,
    500: 3,
    100: 5
  } as Bills) {
    this.bills = bills
  }

  calculateBillsNeeded(amount: number, bills: Array<number>): Bills {
    if (amount === 0) return {}
    if (!bills.length) return

    const bill = bills[0]
    const count = Math.min(this.bills[bill], Math.floor(amount / bill))

    for (let index = count; index >= 0; index--) {
        const result = this.calculateBillsNeeded(amount - index * bill, bills.slice(1))
        if (result) {
          return index ? { [bill]: index, ...result } : result
        }
    }
  }

  withdraw(amount: number): Object {
    const bills = Object.keys(this.bills).filter(bill => this.bills[bill] > 0).map(Number).sort((a, b) => b - a)
    if (bills.length === 0) throw new Error('No bills available to match the requested amount')

    const billsToReturn = this.calculateBillsNeeded(amount, bills)
    if (!billsToReturn) throw new Error('No bills available to match the requested amount')

    for (const key in billsToReturn) {
      if (!billsToReturn.hasOwnProperty(key)) return

      this.bills[key] = this.bills[key] - billsToReturn[key]
    }

    return billsToReturn
  }
}

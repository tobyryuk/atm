import ATM from './main'

test('withdrawals of specified order', () => {
  const atm = new ATM

  expect(atm.withdraw(1500)).toMatchObject({1000: 1, 500: 1})
  expect(atm.withdraw(700)).toMatchObject({500: 1, 100: 2})
  expect(() => {
    atm.withdraw(400)
  }).toThrow()
  expect(atm.withdraw(1100)).toMatchObject({1000: 1, 100: 1})
  expect(() => {
    atm.withdraw(1000)
  }).toThrow()
  expect(atm.withdraw(700)).toMatchObject({500: 1, 100: 2})
  expect(() => {
    atm.withdraw(300)
  }).toThrow()
})

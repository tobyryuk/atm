# ATM

An ATM class which is created with a predetermined amount of bills available. One public function for withdrawing and one recursive function for figuring out if enough bills are available and which to return.

Due to lack of time, parts of the solution was researched from other challenges available online.

### Structure
Built using TypeScript and tested with Jest. Exports an ATM class with an optional parameter (extension from original assignment) which takes a Bills object containing the bill as a key with the amount of bills of that type available as the value. Was a while ago since I used TypeScript so wanted to challenge myself with it.

To run the tests, just run
```
yarn && yarn test
# or
npm i && npm run test
```
